import React, {useEffect, useState} from "react";
import './MainPage.css'

const MainPage: React.FC = () => {
    const [tableData, setTableData] = useState([[0, 0]]);
    const [inputing, setInputing] = useState(true);

    useEffect(() => {
        let copy = tableData.slice();
        for (let i = 0; i < 4; i++) {
            copy[i] = [0, 0, 0, 0];
        }
        setTableData(copy);
        // eslint-disable-next-line
    }, []);


    const newTableData = (i: number, j: number, val: number) => {
        let copy = tableData.slice();
        copy[i][j] = val ? val : 0;
        return copy
    };

    const invisible = {
        display: 'none'
    };


    return (
        <div className="MainPage">
            <div className="DataTableInputs" style={inputing ? {} : invisible}>
                <table>
                    <thead>
                    <tr>
                        <th>Mass (kg)</th>
                        <th>Δv (m/s)</th>
                        <th>Avg. Force (N)</th>
                        <th>Δt (s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    {tableData.map((row: Array<number>, i) => {
                        return (
                            <tr key={i}>
                                {row.map((col, j) => {
                                    return (
                                        <td>
                                            <input type='number' value={col} onChange={event => {setTableData(newTableData(i, j, parseFloat(event.currentTarget.value)));}}/>
                                        </td>
                                    )
                                })}
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
            {inputing ? null : <Anal tableData={tableData} />}
            <button onClick={event => {setInputing(!inputing);}}>
                {inputing ? 'Get Anal' : 'Back to Input'}
            </button>
        </div>
    );
};


interface AnalProps  {
    tableData: Array<Array<number>>
}

const Anal: React.FC<AnalProps> = (props: AnalProps) => {
    const calcJ = (row: Array<number>) => {
        return row[2] * row[3];
    };
    const calcP = (row: Array<number>) => {
        return row[0] * row[1];
    };
    const calcE = (p: number, j: number) => {
        return Math.abs(j - p) / ((j + p) / 2);
    };

    let es: Array<number> = [];

    return (
        <div>
            {props.tableData.map((row, index) => {
                const p = calcP(row);
                const j = calcJ(row);
                const e = calcE(p, j);
                es.push(e);
                return (
                    <div>
                        <h3>Trial: {index + 1}</h3>
                        <p>p = {row[0]} * {row[1]} = {p}</p>
                        <p>J = {row[2]} * {row[3]} = {j}</p>
                        <p>%Error = abs({j} - {p}) / ( ({j} + {p}) / 2 ) = {e * 100} % </p>
                    </div>
                );
            })}
            <div>
                <h3>Total Error</h3>
                <p>({es.map((e, i) => {
                    return (<span>{e}{(i+1 === es.length) ? '' : ' + '}</span>);
                })}) / {es.length} = {es.reduce((t, e) => t+e)/es.length} = {es.reduce((t, e) => t+e)/es.length * 100}%</p>
            </div>


        </div>
    );

};


export default MainPage;

