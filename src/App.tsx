import React from 'react';
import './App.css';
import MainPage from "./components/MainPage";

const App: React.FC = () => {
  return (
    <div className="App">
      <MainPage />
    </div>
  );
};

export default App;
